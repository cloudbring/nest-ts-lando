import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LandoLogger } from './app.logging';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: new LandoLogger(),
  });
  await app.listen(3000);
}
bootstrap();
